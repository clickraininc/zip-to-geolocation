<?php
/**
 * zip-to-geolocation plugin for Craft CMS 3.x
 *
 * A plugin to translate (and cache) zip codes to geolocation data using Google's APIs
 *
 * @link      https://clickrain.com
 * @copyright Copyright (c) 2019 Click Rain
 */

/**
 * zip-to-geolocation en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('zip-to-geolocation', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Click Rain
 * @package   Ziptogeolocation
 * @since     1.0.0
 */
return [
    'zip-to-geolocation plugin loaded' => 'zip-to-geolocation plugin loaded',
];
