<?php
/**
 * zip-to-geolocation plugin for Craft CMS 3.x
 *
 * A plugin to translate (and cache) zip codes to geolocation data using Google's APIs
 *
 * @link      https://clickrain.com
 * @copyright Copyright (c) 2019 Click Rain
 */

namespace clickrain\ziptogeolocation\services;

use clickrain\ziptogeolocation\Ziptogeolocation;

use Craft;
use craft\base\Component;
use craft\elements\Entry;

/**
 * ZipToGeolocationService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Click Rain
 * @package   Ziptogeolocation
 * @since     1.0.0
 */
class ZipToGeolocationService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Ziptogeolocation::$plugin->zipToGeolocationService->exampleService()
     *
     * @return mixed
     */

    public function geocodeLookup($zipcode) {
        $entry_exists = 0;
        $cache = false;
        $section_handle = Ziptogeolocation::$plugin->getSettings()->sectionHandle;

        if (!$section_handle) {
            Craft::error('Section Handle Not Set!');
            die('Section handle is not set in plugin settings');
        }

        // Is this a zip code?
        if ($this->isZipCode($zipcode)) {
            $cache = true;
                       
            // Check if we already have a cache of this information
            $entries = Entry::find()
                ->section($section_handle)
                ->limit(1)
                ->slug($zipcode);

            $entry_exists = $entries->count();
        }

        if ($entry_exists) {
            foreach ($entries->all() as $entry) {
                return (object) [ 
                    'latitude' => $entry->latitude,
                    'longitude' => $entry->longitude,
                    'city' => $entry->city,
                    'state' => $entry->state,
                    'stateLong' => $entry->stateLong,
                    'country' => $entry->country
                ];
            }
        } else {
            return $this->googleGeocodeAPIRequest($zipcode, $cache);
        }
    }

    private function isZipCode($zipcode) {
        return preg_match('/^\d{5}$/', $zipcode);
    }

    // filter the address_components field for type : $type
    private function geoFilter($components, $type) {
        return array_filter($components, function($component) use ($type) {
            return array_filter($component["types"], function($data) use ($type) {
                return $data == $type;
            });
        });
    }

    private function googleGeocodeAPIRequest($zipcode, $cache = true) {
        if (!empty($zipcode)) {
            $zipcode = urlencode($zipcode);
            $apiKey = Ziptogeolocation::$plugin->getSettings()->browserApiKey;
            $section_handle = Ziptogeolocation::$plugin->getSettings()->sectionHandle;

            if (!$apiKey) {
                Craft::error('API Key Not Set!');
                die('API key is not set in plugin settings');
            }

            if (!$section_handle) {
                Craft::error('Section Handle Not Set!');
                die('Section handle is not set in plugin settings');
            }
                    
            $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&key=".$apiKey;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL,$url);
            $result=curl_exec($ch);
            curl_close($ch);

            $data = json_decode($result,true);

            if ($data["results"]) {
                $components = $data["results"][0]["address_components"];

                if (array_values($this->geoFilter($components, "postal_code"))) {
                    $zipcode = array_values($this->geoFilter($components, "postal_code"))[0]["short_name"];
                } else {
                    $zipcode = '';
                }
                
                $city = array_values($this->geoFilter($components, "locality"))[0]["long_name"];
                $state_short = array_values($this->geoFilter($components, "administrative_area_level_1"))[0]["short_name"];
                $state_long = array_values($this->geoFilter($components, "administrative_area_level_1"))[0]["long_name"];
                $country = array_values($this->geoFilter($components, "country"))[0]["short_name"];

                $latitude = $data["results"][0]["geometry"]["location"]["lat"];
                $longitude = $data["results"][0]["geometry"]["location"]["lng"];

                if ($cache && $zipcode && $latitude && $longitude) {
                    // Cache this information in our Zip Codes section
                    $entry = new Entry();
                    $section = Craft::$app->sections->getSectionByHandle($section_handle);
                    $entryTypes = $section->getEntryTypes();
                    $entryType = $entryTypes[0];
                    $entry->sectionId = $section->id;
                    $entry->typeId = $entryType->id;
                    $entry->enabled = true;
                    $entry->siteId = 1;
                    $entry->authorId = 1;
                    $entry->title = $zipcode;
                    $entry->setFieldValues([
                        'zipCode' => $zipcode,
                        'city' => $city,
                        'state' => $state_short,
                        'stateLong' => $state_long,
                        'country' => $country,
                        'latitude' => $latitude,
                        'longitude' => $longitude
                    ]);
                    $success = Craft::$app->elements->saveElement($entry);
                    
                    if (!$success) {
                        Craft::error('Couldn’t save the entry "'.$entry->title.'"', __METHOD__);
                    }
                }

                return (object) [ 
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'city' => $city,
                    'state' => $state_short,
                    'stateLong' => $state_long,
                    'country' => $country
                ];
            } else {
                return '';
            }
        }
    }
}
