<?php
/**
 * zip-to-geolocation plugin for Craft CMS 3.x
 *
 * A plugin to translate (and cache) zip codes to geolocation data using Google's APIs
 *
 * @link      https://clickrain.com
 * @copyright Copyright (c) 2019 Click Rain
 */

namespace clickrain\ziptogeolocation\models;

use clickrain\ziptogeolocation\Ziptogeolocation;

use Craft;
use craft\base\Model;

/**
 * Ziptogeolocation Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Click Rain
 * @package   Ziptogeolocation
 * @since     1.0.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * Some field model attribute
     *
     * @var string
     */
    public $browserApiKey = '';
    public $sectionHandle = '';

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['browserApiKey', 'string'],
            ['browserApiKey', 'default', 'value' => ''],
            ['sectionHandle', 'string'],
            ['sectionHandle', 'default', 'value' => ''],
        ];
    }
}
