<?php
/**
 * zip-to-geolocation plugin for Craft CMS 3.x
 *
 * A plugin to translate (and cache) zip codes to geolocation data using Google's APIs
 *
 * @link      https://clickrain.com
 * @copyright Copyright (c) 2019 Click Rain
 */

namespace clickrain\ziptogeolocation\variables;

use clickrain\ziptogeolocation\Ziptogeolocation;

use Craft;

/**
 * zip-to-geolocation Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.ziptogeolocation }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Click Rain
 * @package   Ziptogeolocation
 * @since     1.0.0
 */
class ZiptogeolocationVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.ziptogeolocation.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.ziptogeolocation.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    /**public function exampleVariable($optional = null)
    *{
    *    $result = "And away we go to the Twig template...";
    *    if ($optional) {
    *        $result = "I'm feeling optional today...";
    *    }
    *    return $result;
    *}
    */

    public function zipToGeolocation($zipcode = null)
    {
        return Ziptogeolocation::$plugin->zipToGeolocationService->geocodeLookup($zipcode);
    }
}
