<?php
/**
 * zip-to-geolocation plugin for Craft CMS 3.x
 *
 * A plugin to translate (and cache) zip codes to geolocation data using Google's APIs
 *
 * @link      https://clickrain.com
 * @copyright Copyright (c) 2019 Click Rain
 */

/**
 * zip-to-geolocation config.php
 *
 * This file exists only as a template for the zip-to-geolocation settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'zip-to-geolocation.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // Set the Google browser API key here
    "browserApiKey" => '',
    "sectionHandle" => '',

];
