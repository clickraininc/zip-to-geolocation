# zip-to-geolocation Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.0.1 - 2020-02-14
### Changed
- Craft 3.4 compatibility

## 1.0.0 - 2019-01-31
### Added
- Initial release
