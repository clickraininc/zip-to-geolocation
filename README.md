# Zip To Geolocation plugin for Craft CMS 3.x

A plugin to translate (and cache) zip codes to geolocation data using Google's APIs

## Requirements

This plugin requires Craft CMS 3.0.0 or later.

## Installation

To install the plugin, follow these instructions:

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Add the following to your project's `composer.json` file:

        "require": {
            "clickrain/zip-to-geolocation": "master@dev"            
        },            
        "repositories": [
            {
                "type": "vcs",
                "url": "https://bitbucket.org/clickraininc/zip-to-geolocation.git"
            }
        ]

3. Run `composer update` from your terminal

4. In the Control Panel, go to Settings → Plugins and click the “Install” button for Zip To Geolocation.

## Zip To Geolocation Overview

Zip To Geolocation provides an easy interface to Google's Geolocation API, with the addition of local caching. When requesting geolocated data for a particular zip code, the plugin first checks a local cache (a Craft Section that must be configured in advance). If an entry exists, the plugin will return the data from within this Section; if it doesn't exist, the plugin will query the Geolocation API and create a new entry within this Section for future use.

## Configuring Zip To Geolocation

This plugin requires both a Craft Section (with defined fields) to be created, as well as plugin settings to be configured:

**Create a new section:**

1. From within the Control Panel, access Settings → Sections
2. Click the 'New section' button
3. Enter a Name (e.g. Zip Codes)
4. Enter a Handle (e.g. zipCodes)
5. Disable entry versioning
6. Click the Save button

**Create fields for use within this section:**

1. From within the Control Panel, access Settings → Fields
2. Choose a field group or create a new one
3. Create the following fields by clicking the New Field button:
   * Zip Code (handle: zipCode; type: plain text)
   * Latitude (handle: latitude; type: plain text)
   * Longitude (handle: longitude; type: plain text)
   * City (handle: city; type: plain text)
   * State (handle: state; type: plain text)
   * State Long (handle: stateLong; type: plain text)
   * Country (handle: country; type: plain text)

**Assign the above fields to the newly created Section:**

1. From within the Control Panel, access Settings → Sections
2. Click 'Edit entry types' to the right of the Section that you created
3. Click the default entry type
4. Verify that 'Show the Title field' is checked
5. From within the 'Design your field layout' section, click the New Tab button
6. Name the tab 'Zip Code Fields'
7. Add the following fields to the tab:
    * Zip Code
    * Latitude
    * Longitude
    * City
    * State
    * State Long
    * Country
8. Click the Save button

**Configure the plugin settings:**

1. From within the Control Panel, access Settings -> Zip To Geolocation
2. Follow the steps at the 'Get an API key' link to generate a new API key and paste this key within the Google Maps API Key field
3. Enter the Section handle that you created from the steps above (e.g. zipCodes)

**Note:** these settings can also be configured by copying the included `config.php` file into your `config` folder and naming it `zip-to-geolocation.php`

You should end up with a new Section whose entry form looks like this:

![Screenshot](resources/img/config.png)

## Using Zip To Geolocation

    {% set geocodedResults = craft.ziptogeolocation.zipToGeolocation('57108') %}
    Latitude: {{ geocodedResults.latitude }}
    Longitude: {{ geocodedResults.longitude }}
    City: {{ geocodedResults.city }}
    State: {{ geocodedResults.state }}
    State (Long): {{ geocodedResults.stateLong }}
    Country: {{ geocodedResults.country }}

## Zip To Location Roadmap

To do:

* Create cache Section and fields automatically upon install OR cache data differently
* Provide config option to flush cache

Brought to you by [Click Rain](https://clickrain.com)
